
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
 function createPatientID(){
     sessionId = getSessionId();
     
     var ime = $("#createName").val();
     var priimek = $("#createLastName").val(); 
     var datumRojstva = $("#create_bDay").val();
    
     if(!ime || !priimek || !datumRojstva || ime.trim().length==0 ||
        priimek.trim().length==0 || datumRojstva.trim().length==0){
            $("#supportMessage").html("<span class='label " +
            "label-warning fade-in'>Please enter all required fields!</span>");
        }else{
            $.ajaxSetup({
               headers: {"Ehr-Session": sessionId} 
            });
            $.ajax({
               url: baseUrl + "/ehr",
               type: 'POST',
               success: function (data) {
                var ehrId = data.ehrId;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                };
                $.ajax({
                   url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                        if(party.action == 'CREATE'){
                            $("#supportMessage").html("<span class='" +
                           "label label-success fade-in'>EHR: '" +
                           ehrId + "'.</span>");
                        }
                    },
                    error: function(err) {
                        $("#supportMessage").html("<span class='label " +
                        "label-danger fade-in'>Error '" +
                        JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
               }
            });
        } 
 };
 
 
 function calculateBMI_addParams(){
   
   sessionId = getSessionId();
   
   var ehrIdP = $("#ehrId_input").val();
   var dateInput = $("#date_input").val();
   var height = $("#weight_input").val();
   var weight = $("#height_input").val();
   
   if(!ehrIdP || !height || !weight || !dateInput || ehrIdP.trim().length==0 || height.trim().length==0 
   || weight.trim().length==0 || dateInput.trim().length==0){
       $("#supportMessageBMI").html("<span class='" +
         "label label-warning fade-in'>Please enter all required fields!</span>");
   }else{
       var bmi = bmiCalulated(weight,height);
       $.ajaxSetup({
             headers: {"Ehr-Session": sessionId}
          });
       var podatki = {
               "ctx/language": "en",
               "ctx/territory": "SI",
               "ctx/time": dateInput,
               "vital_signs/height_length/any_event/body_height_length": height,
               "vital_signs/body_weight/any_event/body_weight": weight,
               "vital_signs/body_temperature/any_event/temperature|magnitude": 0,
		        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		        "vital_signs/blood_pressure/any_event/systolic": 0,
		        "vital_signs/blood_pressure/any_event/diastolic": 0,
		        "vital_signs/indirect_oximetry:0/spo2|numerator": 0
       };
       
       var parametriZahteve = {
           ehrId: ehrIdP,
           templateId: 'Vital Signs',
           format: 'FLAT'
         };
       
       $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki), 
            
             success: function (res) {
                 
		        $("#supportMessageBMI").html(
                "<div class='row' align:'center' style='margin-top:10px'><span class='label label-success fade-in'>" +
                "Your info was successfully stored" + ".</span></div>");
            },
            error: function(err) {
		    $("#supportMessageBMI").html(
            "<span class='label label-danger fade-in'>Error: '" +
            JSON.parse(err.responseText).userMessage + "'!");
            }
       });
   }
 }
 
 function bmiCalulated(weight,height){
     var finalBmi = weight/(height/100*height/100);
        
        if(finalBmi < 18.5){
            $("#bmiValue").html(
              "<span>" +"<strong>You are too thin. Your BMI is: "+finalBmi+"</strong>.</span>");
        }else if(finalBmi > 18.5 && finalBmi < 25){
            $("#bmiValue").html(
              "<span>" +"<strong>You are healthy. Your BMI is: "+finalBmi+"</strong>.</span>");
        }else if(finalBmi > 25){
            $("#bmiValue").html(
              "<span>" +"<strong>You are overweight. Your BMI is: "+finalBmi+"</strong>.</span>");
        }
                
        return finalBmi;
 }
 
 function izris(res,value){
     
    Morris.Line({
  element: 'area-chart',
  data: [
    { y: res[value-5].time.substring(0,10), a: res[value-5].weight},
    { y: res[value-4].time.substring(0,10), a: res[value-4].weight},
    { y: res[value-3].time.substring(0,10), a: res[value-3].weight},
    { y: res[value-2].time.substring(0,10), a: res[value-2].weight},
    { y: res[value-1].time.substring(0,10), a: res[value-1].weight}
  ],
  xkey: 'y',
  ykeys: ['a'],
  xLabels: "day",
  labels: ['Weight']
});
}
 
 function BMIprogress(){
     sessionId = getSessionId();
     
     var ehrId = $("#inputEHR-ID").val();

     if(!ehrId || ehrId.trim().length == 0){
         $("#supportMessage_trackBMI").html("<span class='obvestilo " +
        "label label-warning fade-in'>Please enter your EHR ID!");
     }else{
         $("#BMI_View").modal();
         $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                var party = data.party;
                $("#izpisi").html("<br/><span>Pridobivanje " +
                "podatkov za bolnika <b>" + party.firstNames +
                " " + party.lastNames + "</b> Prosimo pocakajte! </span><br/><br/>");
                $.ajax({
                    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					type: 'GET',
                    headers: {"Ehr-Session": sessionId}, 
                    success: function (res) {
                        if (res.length > 0) {
                                $(".poizvedba").empty();
                                $(".poizvedba").append("<div id='izpisi'></div>");   
                                
						    	var results = "<table class='table table-striped " +
                                             "table-hover'><tr><th>Ime in primek</th><th class'text-center'>Date</th>" +
                                             "<th class='text-right'>Kilaza</th></tr>";
                        for (var i in res) {
						      results += "<tr><td>" + party.firstNames +" "+ party.lastNames+ 
                                        "</td><td>"+res[i].time+"</td><td class='text-right'>" + res[i].weight +" "+res[i].unit+
                                        " " + "</td>";
                            }
                            results += "</table>";
                            
                            $("#izpisi").append(results);
                        }else{
                            $("#izpisi").html(
                            "<span class='label label-warning fade-in'>" +
                            "No data was added yet.</span>");
                        }
                        
                        setTimeout(function(){ 
                            $(".createChart").empty();
                            $(".createChart").append("<div id='area-chart'></div>"); 
                            
                            if(res.length>=5){
                                izris(res,res.length);
                            }else{
                                $("#area-chart").html(
                                "<h1><span class='label label-warning fade-in'>" +
                                "Atleast 5 inputs for a graph.</span></h1>");
                            }
                            
                        }, 2000);
                        
     
                    },
                    error: function(err) {
					    	$("#izpisi").html(
                            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                             JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
            },
            error: function(err) {
	    		$("#izpisi").html(
                "<span class='label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
                }
         });
     }
 }
 
 function health_addParams(){
     sessionId = getSessionId();

	var ehrId = $("#ehrId_health_input").val();
	var datumInUra = $("#date_health_input").val();
	var telesnaTemperatura = $("#temperature_input").val();
	var sistolicniKrvniTlak = $("#systolic_input").val();
	var diastolicniKrvniTlak = $("#diastolic_input").val();
	var nasicenostKrviSKisikom = $("#oxygen_input").val();

    if (!ehrId || !datumInUra || !telesnaTemperatura || !sistolicniKrvniTlak || !diastolicniKrvniTlak ||
    !nasicenostKrviSKisikom || ehrId.trim().length == 0 || datumInUra.trim().length == 0 || telesnaTemperatura.trim().length == 0
    || sistolicniKrvniTlak.trim().length == 0 || diastolicniKrvniTlak.trim().length == 0 || nasicenostKrviSKisikom.trim().length == 0) {
		$("#supportMessageHealth").html("<span class='" +
      "label label-warning fade-in'>Please enter all required fields!</span>");
    }else{
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
        });
        
        var podatki = {
            "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        };
        var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
        };
        $.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#supportMessageHealth").html(
              "<span class='obvestilo label label-success fade-in'>" +
              "Your information was successfully stored" + ".</span>");
		    },
		    error: function(err) {
		    	$("#supportMessageHealth").html(
            "<span class='obvestilo label label-danger fade-in'>Error: '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
        });
    }
}
 
function HealthProgress(){
    
    sessionId =getSessionId();
    
    var ehrId = $("#inputEHR-ID").val();
    
    if(!ehrId || ehrId.trim()==0){
        $("#supportMessage_trackBMI").html("<span class='" +
        "label label-warning fade-in'>Please enter your EHR ID!");
    }else{
       $.ajaxSetup({
	    headers: {
	        "Ehr-Session": sessionId
	    }
	});

	var aql = "SELECT " +
    "a/content[openEHR-EHR-OBSERVATION.body_temperature.v1]/data[at0002]/events[at0003]/data[at0001]/items[at0004] as telTempVr, "+
    "a/content[openEHR-EHR-OBSERVATION.body_temperature.v1]/data[at0002]/events[at0003]/time as telTempCas, "+
    "a/content[openEHR-EHR-OBSERVATION.blood_pressure.v1]/data[at0001]/events[at0006]/data[at0003] as telKrvniTlak, "+
    "a/content[openEHR-EHR-OBSERVATION.blood_pressure.v1]/data[at0001]/events[at0006]/time as telKrvniTlakCas, " +
    "a/content[openEHR-EHR-OBSERVATION.height.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0004] as telVisinaVr, "+
    "a/content[openEHR-EHR-OBSERVATION.height.v1]/data[at0001]/events[at0002]/time as telVisinaCas, "+
    "a/content[openEHR-EHR-OBSERVATION.body_weight.v1]/data[at0002]/events[at0003]/data[at0001]/items[at0004] as telTezaVr, "+
    "a/content[openEHR-EHR-OBSERVATION.indirect_oximetry.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0006] as nasicKrviSKisikomVr, "+
    "a/content[openEHR-EHR-OBSERVATION.indirect_oximetry.v1]/data[at0001]/events[at0002]/time as nasicKrviSKisikomCas, "+
    "a/territory/code_string as territory_code_string "+
	"from EHR e "+
	"contains COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] "+
	"where "+
	"a/name/value='Vital Signs' and "+
	"e/ehr_id/value='"+ ehrId +"' "+
	"offset 0 limit 100";	

	console.log(aql);
	$.ajax({
	    url: baseUrl + "/query?" + $.param({"aql": aql}),
	    type: 'GET',
	    success: function (res) {
	        $("#healthView").modal();
	        if(res != null){
	        	var rows = res.resultSet;
                izlusciPodatkeVitalnihZnakov(rows);
	        }else{
	        	$("#izpisiH").html(
                            "<span class='label label-warning fade-in'>" +
                            "No data was added yet.</span>");
	        }
	    },
	    error: function(err) {
			console.log(JSON.parse(err.responseText).userMessage);
		}
	}); 
    }
}

function izris2(res,value){
     
     var array = new Array(value);
     
     for(var i = 0;i<value;i++){
         array[i]=new Array(3);
     }
     
     var count = 0;
     
     for(var i in res){
        if(res[i].telKrvniTlakCas.value.substr(0,10)!=0 && res[i].telKrvniTlak.items[0].value.magnitude!=0 && res[i].telKrvniTlak.items[1].value.magnitude!=0){
            array[count][0]=res[i].telKrvniTlakCas.value.substr(0,10);
            array[count][1]=res[i].telKrvniTlak.items[0].value.magnitude;
            array[count][2]=res[i].telKrvniTlak.items[1].value.magnitude;
            count++;
        }
     }
     
   var data = [
      { y: array[count-5][0], a: array[count-5][1],  b: array[count-5][2]},
      { y: array[count-4][0], a: array[count-4][1],  b: array[count-4][2]},
      { y: array[count-3][0], a: array[count-3][1],  b: array[count-3][2]},
      { y: array[count-2][0], a: array[count-2][1],  b: array[count-2][2]},
      { y: array[count-1][0], a: array[count-1][1],  b: array[count-1][2]}
    ],
    config = {
      data: data,
      xkey: 'y',
      ykeys: ['a','b'],
      labels: ['Systolic','Diastolic'],
      fillOpacity: 0.6,
      hideHover: 'auto',
      behaveLikeLine: true,
      resize: true,
      pointFillColors:['#ffffff'],
      pointStrokeColors: ['black'],
  };
config.element = 'bar-chart';
Morris.Bar(config);
}

function izlusciPodatkeVitalnihZnakov(rows){
	
	 $(".poizvedbaH").empty();
     $(".poizvedbaH").append("<div id='izpisiH'></div>"); 
    
    	var results = "<table class='table table-striped " +
                      "table-hover'><tr><th>Date</th><th>Temperature</th>" +
                      "<th>Systolic</th><th class='text-right'>Diastolic</th></tr>";

	for(var key in rows){	

		var vrednostMeritveTemp = rows[key].telTempVr.value.magnitude;
		var vrednostMeritveSisTlaka = rows[key].telKrvniTlak.items[0].value.magnitude;
		var vrednostMeritveDiasTlaka = rows[key].telKrvniTlak.items[1].value.magnitude;
		var casMeritveKrvnegaTlaka = rows[key].telKrvniTlakCas.value;
		
		if(!vrednostMeritveTemp || !vrednostMeritveSisTlaka || !vrednostMeritveDiasTlaka || !casMeritveKrvnegaTlaka){
		    
		}else{
		    results += "<tr><td>" + casMeritveKrvnegaTlaka+"</td><td>"+" "+ vrednostMeritveTemp+ 
                       " °C</td><td>"+vrednostMeritveSisTlaka+" mm/Hg </td><td class='text-right'>" + vrednostMeritveDiasTlaka +" mm/Hg "+
                       " " + "</td>";
		}
     }
     
        results += "</table>";
     
        $("#izpisiH").append(results);
		
		if(rows.length<5){
		    $("#bar-chart").html("<h1><span class='label label-warning fade-in'>" +
                                 "Atleast 5 inputs for a graph.</span></h1>");
		}else{
		    $(".createChartss").empty();
            $(".createChartss").append("<div id='bar-chart'></div>"); 
            setTimeout(function(){ izris2(rows,rows.length); }, 1000);
		}
}

function weatherAPI(){
   
     var cityName = $("#city_id").val();;
     var weatherUrl = "https://api.apixu.com/v1/current.json?key=1c6a3cc1329642aaac7205823172705&q="+cityName;
     
     if(!cityName || cityName.trim().length==0){
         $("#supportMessage_weather").html("<span class='" +
         "label label-warning fade-in'>Please enter a city!</span>");
     }else{
        $("#supportMessage_weather").empty();
        $.ajax({
         url: weatherUrl,
         type: 'GET',
		 success: function(res){
 
            $("#supportMessage_weather").empty();
            
            $('#weatherAPI_output').modal('hide');
		     
		    $(".vnosi").empty(); 
		     
		     var results = "<table class='table table-striped " +
                           "table-hover'><tr><th align='center'>Information</th>" +
                           "<th></th></tr>";
                           
			results += "<tr><td><b>City</b> "+ res.location.name+ 
                       "</td><td><b>Country</b> "+res.location.country+"</td>";
            results += "<tr><td><b>Current Weather</b> "+res.current.condition.text+ 
                       "</td><td><b>Humidity</b> "+res.current.humidity+"%</td>";
            results += "<tr><td><b>Temperature</b> "+ res.current.temp_c+ 
                       " °C</td><td><b>Wind speed</b> "+res.current.wind_kph+" km/h</td>";
                            
           results += "</table>";
           $(".vnosi").append(results);
		     
		 },
		 error: function(err){
		     $("#supportMessage_weather").html("<span class='" +
            "label label-danger fade-in'>Not a valid city.</span></br>");
		 }
     });   
     }
}
 
 var ehrIdOseb = new Array(3);
 var os = 0;
 
  function generirajPodatke(oseba,callback){
     sessionId = getSessionId();
     
        var ime = oseba[0];
        var priimek = oseba[1]; 
        var datumRojstva = oseba[2];
    
            $.ajaxSetup({
               headers: {"Ehr-Session": sessionId} 
            });
            $.ajax({
               url: baseUrl + "/ehr",
               type: 'POST',
               success: function (data) {
                var ehrId = data.ehrId;
                callback(ehrId);
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                };
                $.ajax({
                   url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                    },
                    error: function(err) {
                    }
                });
               }
            });
 };
 
 function generateWeight(eID,arr,i,j){
   
   sessionId = getSessionId();

        var height = arr[j][1][i];
        var dateInput = arr[j][2][i];
        var weight = arr[j][0][i];

       $.ajaxSetup({
             headers: {"Ehr-Session": sessionId}
          });
       var podatki = {
               "ctx/language": "en",
               "ctx/territory": "SI",
               "ctx/time": dateInput,
               "vital_signs/height_length/any_event/body_height_length": height,
               "vital_signs/body_weight/any_event/body_weight": weight,
               "vital_signs/body_temperature/any_event/temperature|magnitude": 0,
		        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		        "vital_signs/blood_pressure/any_event/systolic": 0,
		        "vital_signs/blood_pressure/any_event/diastolic": 0,
		        "vital_signs/indirect_oximetry:0/spo2|numerator": 0
       };
       
       var parametriZahteve = {
           ehrId: eID,
           templateId: 'Vital Signs',
           format: 'FLAT'
         };
       
       $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki), 
            
             success: function (res) {
             },
             error: function(err) {
             }
       });   
 }
 
 function generateHealth(eID,arr,i,j){
     sessionId = getSessionId();

	var datumInUra = arr[j][2][i];
	var telesnaTemperatura = arr[j][3][i];
	var sistolicniKrvniTlak = arr[j][4][i];
	var diastolicniKrvniTlak = arr[j][5][i];
	var nasicenostKrviSKisikom = arr[j][6][i];

        $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
        });
        
        var podatki = {
            "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        };
        var parametriZahteve = {
		    ehrId: eID,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
        };
        $.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		    },
		    error: function(err) {
		    }
        });
}
 
 function predProc(){
     
     
     weatherAPI();
     
    var oseba = new Array(3);
    var arr = new Array(3);
     for(var i = 0;i<3;i++){
        oseba[i]=new Array(3);    
        arr[i]=new Array(7);
       
        for(var j = 0;j<7;j++){
            arr[i][j]=new Array(5);
     }
}
     
     /*MY TIME TO SHINE BABY*/
     oseba[0][0] = 'Bill';
     oseba[0][1] = 'Mandal';
     oseba[0][2] = '1967-04-10T11:08';
     arr[0][0][0] = '90'; arr[0][0][1] = '95'; arr[0][0][2] = '97'; arr[0][0][3] = '92'; arr[0][0][4] = '85';
     arr[0][1][0] = '190'; arr[0][1][1] = '192'; arr[0][1][2] = '191'; arr[0][1][3] = '189'; arr[0][1][4] = '190';
     arr[0][2][0] = '2015-04-10T11:08'; arr[0][2][1] = '2015-06-10T11:09'; arr[0][2][2] = '2015-08-10T13:08'; arr[0][2][3] = '2015-09-10T12:11'; arr[0][2][4] = '2015-11-05T22:18';
     arr[0][3][0] = '36'; arr[0][3][1] = '37'; arr[0][3][2] = '37'; arr[0][3][3] = '38'; arr[0][3][4] = '37';
     arr[0][4][0] = '100'; arr[0][4][1] = '123'; arr[0][4][2] = '120'; arr[0][4][3] = '111'; arr[0][4][4] = '115';
     arr[0][5][0] = '86'; arr[0][5][1] = '87'; arr[0][5][2] = '90'; arr[0][5][3] = '84'; arr[0][5][4] = '88';
     arr[0][6][0] = '90'; arr[0][6][1] = '95'; arr[0][6][2] = '97'; arr[0][6][3] = '92'; arr[0][6][4] = '95';
     
     oseba[1][0] = 'Joe';
     oseba[1][1] = 'Gorge';
     oseba[1][2] = '1988-09-8T13:08';
     arr[1][0][0] = '66'; arr[1][0][1] = '65'; arr[1][0][2] = '64'; arr[1][0][3] = '77'; arr[1][0][4] = '78';
     arr[1][1][0] = '159'; arr[1][1][1] = '160'; arr[1][1][2] = '166'; arr[1][1][3] = '166'; arr[1][1][4] = '165';
     arr[1][2][0] = '2017-02-10T11:08'; arr[1][2][1] = '2017-03-10T11:09'; arr[1][2][2] = '2017-08-10T13:08'; arr[1][2][3] = '2017-09-10T12:11'; arr[1][2][4] = '2017-11-05T22:18';
     arr[1][3][0] = '36.6'; arr[1][3][1] = '37'; arr[1][3][2] = '37.2'; arr[1][3][3] = '38'; arr[1][3][4] = '37.1';
     arr[1][4][0] = '105'; arr[1][4][1] = '113'; arr[1][4][2] = '111'; arr[1][4][3] = '101'; arr[1][4][4] = '110';
     arr[1][5][0] = '90'; arr[1][5][1] = '93'; arr[1][5][2] = '94'; arr[1][5][3] = '94'; arr[1][5][4] = '89';
     arr[1][6][0] = '91'; arr[1][6][1] = '92'; arr[1][6][2] = '99'; arr[1][6][3] = '91'; arr[1][6][4] = '93';
     
     oseba[2][0] = 'Alex';
     oseba[2][1] = 'Naval';
     oseba[2][2] = '1943-03-11T15:08';
     arr[2][0][0] = '66'; arr[2][0][1] = '65'; arr[2][0][2] = '64'; arr[2][0][3] = '77'; arr[2][0][4] = '78';
     arr[2][1][0] = '159'; arr[2][1][1] = '160'; arr[2][1][2] = '166'; arr[2][1][3] = '166'; arr[2][1][4] = '165';
     arr[2][2][0] = '2017-02-10T11:08'; arr[2][2][1] = '2017-03-10T11:09'; arr[2][2][2] = '2017-08-10T13:08'; arr[2][2][3] = '2017-09-10T12:11'; arr[2][2][4] = '2017-11-05T22:18';
     arr[2][3][0] = '36.7'; arr[2][3][1] = '37.3'; arr[2][3][2] = '37'; arr[2][3][3] = '38'; arr[2][3][4] = '37.9';
     arr[2][4][0] = '102'; arr[2][4][1] = '123'; arr[2][4][2] = '121'; arr[2][4][3] = '115'; arr[2][4][4] = '119';
     arr[2][5][0] = '86'; arr[2][5][1] = '90'; arr[2][5][2] = '89'; arr[2][5][3] = '85'; arr[2][5][4] = '90';
     arr[2][6][0] = '91'; arr[2][6][1] = '92'; arr[2][6][2] = '93'; arr[2][6][3] = '92'; arr[2][6][4] = '91';
     
    generirajPodatke(oseba[0],function(rez){
        ehrIdOseb[os%3]=rez;
        os++;
    });
   generirajPodatke(oseba[1],function(rez){
        ehrIdOseb[os%3]=rez;
        os++;
    });
    generirajPodatke(oseba[2],function(rez){
        ehrIdOseb[os%3]=rez;
        os++;
    });
    
    setTimeout(function(){ 
            for(var i = 0;i<5;i++){
            generateWeight(ehrIdOseb[0],arr,i,0); 
            generateHealth(ehrIdOseb[0],arr,i,0);
            }
    }, 1000);
    
    setTimeout(function(){ 
            for(var i = 0;i<5;i++){
            generateWeight(ehrIdOseb[1],arr,i,1);  
            generateHealth(ehrIdOseb[1],arr,i,1);
            }
    }, 1000);
    
    setTimeout(function(){ 
            for(var i = 0;i<5;i++){
            generateWeight(ehrIdOseb[2],arr,i,2);
            generateHealth(ehrIdOseb[2],arr,i,2);
            }
    }, 1000);
    
    setTimeout(function() {
        $(".gen").empty(); 
        $(".gen").append("<p class='col'><b>Patient: Bill Mandal, EhrID: "+ehrIdOseb[0]+"</b></p></br>"+
        "<p class='col'><b>Patient: Joe Gorge, EhrID: "+ehrIdOseb[1]+"</b></p></br>"+
        "<p class='col'><b>Patient: Alex Naval, EhrID: "+ehrIdOseb[2]+"</b></p>");
    },1000);
}

 
 $(document).ready(function(){
    setTimeout(function() {
        $("#weatherAPI_output").modal({backdrop: "static"});
    },3000);
});

